# Geração de Assinatura CMS com BRy Extension - Backend

Este é um exemplo de integração dos serviços da API de assinatura com clientes baseados em tecnologia Java para geração de assinatura CMS. 

Este exemplo é integrado com o [frontend] de geração de assinatura. 

A aplicação realiza comunicação com o serviço de assinatura do Framework.

### Tech

O exemplo utiliza a biblioteca Java abaixo:
* [JDK 8] - Java 8
* [Spring-boot] - Spring Boot makes it easy to create stand-alone, production-grade Spring based Applications that you can "just run".

### Uso

Para execução da aplicação de exemplo, importe o projeto em sua IDE de preferência (utilizamos o Eclipse).

Executar programa:

	clique com o botão direito em cima do arquivo CMSSignatureApplication.java -> Run as -> Java Application

   [JDK 8]: <https://www.oracle.com/java/technologies/javase-jdk8-downloads.html>
   [Spring-boot]: <https://spring.io/projects/spring-boot>
   [frontend]: <https://gitlab.com/brytecnologia-team/integracao/api-assinatura/javascript/frontend-assinatura-cms-com-bry-extension>
   