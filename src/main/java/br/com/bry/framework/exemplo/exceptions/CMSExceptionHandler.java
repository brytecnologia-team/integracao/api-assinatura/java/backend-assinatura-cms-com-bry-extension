package br.com.bry.framework.exemplo.exceptions;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;

import br.com.bry.framework.exemplo.exceptions.ErrorResponse.Error;
import br.com.bry.framework.exemplo.exceptions.ErrorResponse.ErrorBuilder;

@ControllerAdvice
public class CMSExceptionHandler {

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorResponse> handlerExcecaoCMS(final Exception e, final HttpServletRequest request) {
		Error error;
		String key = "error.cms.signature";
		int httpStatus = 500;
		if (e instanceof HttpClientErrorException) {
			HttpClientErrorException httpError = (HttpClientErrorException) e;
			httpStatus = httpError.getRawStatusCode();
			error = new Error(key, httpError.getMessage());
		} else {
			error = new Error(key, e.getMessage());
		}
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
				.body(new ErrorBuilder().status(httpStatus).error(error).timestamp(System.currentTimeMillis()).build());
	}

}
