package br.com.bry.framework.exemplo.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.bry.framework.exemplo.models.FinalizationResponse;
import br.com.bry.framework.exemplo.service.SignatureFinalizationService;

@RestController
public class FinalizationController {

	@Autowired
	private SignatureFinalizationService finalizationService;

	/**
	 * Back-end finalization endpoint
	 * 
	 * @param request
	 * @param response
	 * @return Finalization response
	 * @throws Exception
	 */
	@PostMapping(value = "/finalize", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<FinalizationResponse> finalizeSignature(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		return new ResponseEntity<>(finalizationService.finalizeSignature(request), HttpStatus.OK);
	}

}
