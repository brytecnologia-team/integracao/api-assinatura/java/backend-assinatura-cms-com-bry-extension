package br.com.bry.framework.exemplo.models;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class FinalizationResponse {

	private List<Signature> signatures;

	private BigInteger nonce;

	public void setSignatures(List<Signature> assinaturas) {

		this.signatures = assinaturas;
	}

	public List<Signature> getSignatures() {
		return signatures;
	}

	public void addSignature(Signature assinatura) {
		if (this.signatures == null) {
			this.signatures = new ArrayList<Signature>();
		}
		this.signatures.add(assinatura);
	}

	public BigInteger getNonce() {
		return nonce;
	}

	public void setNonce(BigInteger nonce) {
		this.nonce = nonce;
	}

	@Override
	public String toString() {
		return "FinalizationResponse [signatures=" + signatures + "]";
	}
}
