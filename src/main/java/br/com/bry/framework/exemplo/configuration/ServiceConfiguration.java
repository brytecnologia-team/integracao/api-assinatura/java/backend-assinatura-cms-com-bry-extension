package br.com.bry.framework.exemplo.configuration;

public class ServiceConfiguration {

	private ServiceConfiguration() {

	}

	public static final String SERVICE_BASE_URL = "https://fw2.bry.com.br";
	public static final String INITIALIZE_SERVICE_URL = SERVICE_BASE_URL
			+ "/api/cms-signature-service/v1/signatures/initialize";
	public static final String FINALIZE_SERVICE_URL = SERVICE_BASE_URL
			+ "/api/cms-signature-service/v1/signatures/finalize";
}
