package br.com.bry.framework.exemplo.models;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class InitializationResponse {

	private HashAlgorithm hashAlgorithm;

	private List<SignedAttributes> signedAttributes;

	private BigInteger nonce;

	public List<SignedAttributes> getSignedAttributes() {
		return signedAttributes;
	}

	public void setSignedAttributes(List<SignedAttributes> signedAttributes) {
		this.signedAttributes = signedAttributes;
	}

	public void addSignedAttribute(SignedAttributes signedAttribute) {
		if (this.signedAttributes == null) {
			this.signedAttributes = new ArrayList<SignedAttributes>();
		}
		this.signedAttributes.add(signedAttribute);
	}

	public HashAlgorithm getHashAlgorithm() {
		return hashAlgorithm;
	}

	public void setHashAlgorithm(HashAlgorithm hashAlgorithm) {
		this.hashAlgorithm = hashAlgorithm;
	}

	public BigInteger getNonce() {
		return nonce;
	}

	public void setNonce(BigInteger nonce) {
		this.nonce = nonce;
	}

	public String toString() {
		return "InitializationResponse [" + "nonce=" + nonce + ", signedAttributes=" + signedAttributes
				+ ", hashAlgorithm=" + hashAlgorithm + "]";
	}
}